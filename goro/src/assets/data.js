const defaultData = [
  {
    category: "New Songs",
    songs: [
      { title: 'Fresh Caps - Text me your problems'},
      { title: 'LØLØ - 2 of us'},
      { title: 'Fireboy DML - Obaa Sima'},
      { title: 'AUR, ZAYN - Tu hai kahan (Remix)'},
      { title: 'Monoir, Iuliana Beregoi - Taka Taki'},
      { title: 'Unknown T - Till We Meet Again'},
      { title: 'Unknown T - Hard Life'},
      { title: 'Unknown T, GRM Daily - Like SZA'},
      { title: 'Natalija Bunkė - Aš gyvenu (Dainos tekstas)'},
      { title: 'Yung Bleu - Vent'},
    ]
  },
  {
    category: "Popular Songs",
    songs: [
      { title: 'Fresh Caps - Text me your problems'},
      { title: 'LØLØ - 2 of us'},
      { title: 'Fireboy DML - Obaa Sima'},
      { title: 'AUR, ZAYN - Tu hai kahan (Remix)'},
      { title: 'Monoir, Iuliana Beregoi - Taka Taki'},
      { title: 'Unknown T - Till We Meet Again'},
      { title: 'Unknown T - Hard Life'},
      { title: 'Unknown T, GRM Daily - Like SZA'},
      { title: 'Natalija Bunkė - Aš gyvenu (Dainos tekstas)'},
      { title: 'Yung Bleu - Vent'},
    ],
  },
  {
    category: "Favorite Songs",
    songs: [
      { title: 'Fresh Caps - Text me your problems'},
      { title: 'LØLØ - 2 of us'},
      { title: 'Fireboy DML - Obaa Sima'},
      { title: 'AUR, ZAYN - Tu hai kahan (Remix)'},
      { title: 'Monoir, Iuliana Beregoi - Taka Taki'},
      { title: 'Unknown T - Till We Meet Again'},
      { title: 'Unknown T - Hard Life'},
      { title: 'Unknown T, GRM Daily - Like SZA'},
      { title: 'Natalija Bunkė - Aš gyvenu (Dainos tekstas)'},
      { title: 'Yung Bleu - Vent'},
    ],
  },
];

const topSingers = [
  {
    name: "Top Singers day",
    artist: [
      { title: 'Drake'},
      { title: 'Lil Mabu'},
      { title: 'Chriseanrock'},
      { title: 'Jorge Rivera-Herrans'},
      { title: 'Monoir'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Natalija Bunkė'},
      { title: 'Yung Bleu'},
    ]
  },
  {
    name: "Top Singers week",
    artist: [
      { title: 'Drake'},
      { title: 'Lil Mabu'},
      { title: 'Chriseanrock'},
      { title: 'Jorge Rivera-Herrans'},
      { title: 'Monoir'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Natalija Bunkė'},
      { title: 'Yung Bleu'},
    ]
  },
  {
    name: "Top Singers mounths",
    artist: [
      { title: 'Drake'},
      { title: 'Lil Mabu'},
      { title: 'Chriseanrock'},
      { title: 'Jorge Rivera-Herrans'},
      { title: 'Monoir'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Unknown'},
      { title: 'Natalija Bunkė'},
      { title: 'Yung Bleu'},
    ]
  },
];


export { defaultData, topSingers };