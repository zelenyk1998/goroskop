import React, { useState } from 'react';
import { defaultData, topSingers } from "../assets/data.js";
import "./navbar.style.css";
import AlphabetNav from "./alphabetNav";

const Home = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedCategory, setSelectedCategory] = useState('Всі');
  const cardsPerPage = 12;


const renderItemMusic = (category) => (
    <div style={styles.card} key={category.category}>
      <h2 style={styles.additionalInfoTextCenter}>{category.category}</h2>
      <div style={styles.songsContainer}>
        {category.songs.map((song, index) => (
          <div key={index} style={styles.songItems}>
            <p style={styles.name}>{song.title}</p>
          </div>
        ))}
      </div>
    </div>
  );
const renderItem = (name) => (
    <div style={styles.card} key={name.name}>
    <h2 style={styles.additionalInfoTextCenter}>{name.name}</h2>
      <div style={styles.songsContainer}>
        {name.artist.map((artists, index) => (
          <div key={index} style={styles.songItems}>
            <p style={styles.name}>{artists.title}</p>
          </div>
        ))}
      </div>
    </div>
  );

  return (
    <div className="Home" style={styles.container}>
      <div>
        <nav id="main-navbar">
          <div className="container">
            <h2>ТЕКСТІКІ</h2>
            <ul>
              <li>
                <a href="/home">Popular</a>
              </li>
              <li>
                <a href="/home">New</a>
              </li>
              <li>
                <a href="/home">Favorite</a>
              </li>
              <input style={styles.search} placeholder="Search"/>
            </ul>
          </div>
        </nav>
      </div>
      <main style={styles.body}>
        <div style={styles.content}>
          {defaultData.map((item, index) => (
            renderItemMusic(item)
          ))}
        </div>
      </main>
      <AlphabetNav/>
      <div style={styles.bodys}>
        <div style={styles.content}>
          {topSingers.map((item, index) => (
            renderItem(item)
          ))}
        </div>
      </div>
      <footer style={styles.footer}>
        <p style={styles.footerText}>Ваша компанія</p>
        <p style={styles.footerText}>Адреса: вул. Ваша адреса, місто</p>
        <p style={styles.footerText}>Телефон: +380123456789</p>
        <p style={styles.footerText}>Email: your@email.com</p>
      </footer>
    </div>
  );
};

export default Home;


const styles = {
  container: {
    backgroundColor: "white",
    padding: "10px",
  },
  categoryContainer: {
    flexDirection: "center",
    display: "flex",
    flexWrap: "wrap",
  },
  body: {
    minHeight: "65vh",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    backgroundColor: "white",
  },
  bodys:{
    minHeight: "65vh",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    backgroundColor: "white",
  },
  content: {
    minHeight: "65vh",
    width: "85%",
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    backgroundColor: "white",
    flexWrap: "wrap",
  },
  header: {
    borderRadius: "5px",
    border: "1px solid #ccc",
    backgroundColor: "#9370DB",
    textAlign: "center",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  footer: {
    backgroundColor: "#6495ED",
    textAlign: "center",
    height: "20vh",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10px",
    flexDirection: "column",
    borderRadius: 5,
  },
  textGov: {
    fontSize: "30px",
    color: "white",
  },
categories: {
    marginTop: "2%",
    backgroundColor: "#9370DB",
    display: "flex",
    flexWrap: "wrap",
    gap: "10px",
    marginBottom: "10px",
    alignItems: "center",
    justifyContent: "flex-end", // Змінено на "flex-end"
},
  buttons: {
    backgroundColor: "#9370DB",
    color: "black",
    padding: "10px 20px",
    margin: "0 5px",
    cursor: "pointer",
    maxWidth: "120px",
    border: "2px solid white",
    borderRadius: "10px",
  },
search: {
    width: "30vh",
    padding: "5px",
    borderRadius: "5px",
    border: "1px solid #ccc",
    marginLeft: "auto", // Додано "marginLeft: auto;"
},
  card: {
    backgroundColor: "#F5F5F5",
    borderRadius: "10px",
    padding: "20px",
    margin: "10px",
    width: "100%",
    maxWidth: "450px",
    flexDirection: "column",
    textAlign: "left",
  },
  additionalInfoTextCenter: {
    color: "#2F4F4F",
    marginBottom: "10px",
    textAlign: "center",
  },
  songsContainer: {
    textAlign: "center",
  },
  songItems: {
    marginTop: 30,
  },
  name: {
    marginTop: 5,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    textAlign: "left",
    borderBottom: "1px solid white",
  },
};
