import React from 'react';
import './alphabetNav.css';

function AlphabetNav() {
  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

  return (
    <div className="alphabet-nav">
    <div>
    <h3>Search by first letter of artist:</h3>
    </div>
    <div>
      {alphabet.map((letter, index) => (
        <button key={index}>{letter}</button>
      ))}
      </div>
    </div>
  );
}

export default AlphabetNav;