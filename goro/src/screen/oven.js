import React, { useState } from 'react';
import { defaultData } from "../assets/data.js";

const Oven = () => {
  // Знайдіть дані для Овна
  const ovenData = defaultData.find(item => item.key === 'Овен');

  // Додайте стан для вибору інших гороскопів
  const [selectedSign, setSelectedSign] = useState('Овен');

  // Обробник зміни вибраного знаку зодіаку
  const handleSignChange = (e) => {
    setSelectedSign(e.target.value);
  };

  return (
    <div style={styles.container}>
      <h1 style={styles.header}>Гороскоп - Овен</h1>
      <h2 style={styles.selectHeader}>Виберіть інший знак зодіаку:</h2>
      <select
        value={selectedSign}
        onChange={handleSignChange}
        style={styles.selectCategory}
      >
        <option value="Овен">Овен</option>
        <option value="Телец">Телец</option>
        <option value="Близнецы">Близнецы</option>
        {/* Додайте інші опції для знаків зодіаку */}
      </select>
      <div style={styles.ovenInfo}>
        <img src={ovenData.image} style={styles.imageBrand} alt={ovenData.key} />
        <div style={styles.additionalInfoContainer}>
          <h2 style={styles.additionalInfoTextCenter}>{ovenData.key}</h2>
          <p style={styles.opus}>{ovenData.info}</p>
          <span style={styles.priceText}>{ovenData.date}</span>
        </div>
      </div>
    </div>
  );
};

// Решта коду залишається незмінним

export default Oven;

const styles = {
  container: {
    backgroundColor: "#ADD8E6",
    padding: "10px",
  },
  header: {
    backgroundColor: "#ADD8E6",
    textAlign: "center",
    width: "100%",
    color: "#9370DB",
  },
  selectHeader: {
    marginTop: "20px",
  },
  ovenInfo: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "20px",
  },
  additionalInfoContainer: {
    textAlign: "left",
  },
  selectCategory: {
    padding: "15px",
    margin: "10px",
    borderRadius: "5px",
    border: "1px solid #ccc",
    backgroundColor: "#fff",
    fontSize: "16px",
    maxWidth: "100vh",
  },
  imageBrand: {
    width: "320px",
    maxWidth: "100%",
    height: "auto",
  },
  additionalInfoTextCenter: {
    color: "#2F4F4F",
  },
  opus: {
    color: "#696969",
  },
  priceText: {
    color: "#9370DB",
  },
};

