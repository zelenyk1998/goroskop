import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from "./screen/home.js"; 
import Oven from "./screen/oven.js";


const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/oven" element={<Oven />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
